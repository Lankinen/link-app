DROP TABLE IF EXISTS links;

CREATE TABLE links (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT,
  url TEXT,
  clicks INTEGER DEFAULT 0
);

/* FOR TESTING PURPOSE INSERT SOME DATA */
INSERT INTO links (title, url) VALUES ("CNN", "https://edition.cnn.com/");
INSERT INTO links (title, url) VALUES ("BBC", "https://www.bbc.co.uk/");
INSERT INTO links (title, url) VALUES ("ABC", "https://abc.com/");