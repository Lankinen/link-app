### Install required modules

`pip install -r requirements.txt`

### Initialize database

```
$ Python
> from app import init_db
> init_db()
```

### Run

`flask run`
