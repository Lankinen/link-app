import sqlite3

from flask import Flask, request, jsonify
from flask_cors import CORS

DATABASE = 'linkapp.db'

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


def get_db():
    return sqlite3.connect(DATABASE)


def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
    return "done!"


@app.route("/links/", methods=["GET", "OPTIONS"])
def get_all():
    cur = get_db().cursor()
    cur.execute("SELECT * FROM links")
    rows = cur.fetchall()
    return {"result": rows}


@app.route("/link/", methods=["POST"])
def add_link():
    data = request.get_json()
    title = data.get("title", "")
    url = data.get("url", "")
    db = get_db()
    cur = db.cursor()
    result = cur.execute(
        "INSERT INTO links (title, url) VALUES (?, ?)", (title, url))
    db.commit()
    return {"result": result.lastrowid}


@app.route("/link/<id>", methods=["DELETE"])
def delete(id):
    db = get_db()
    cur = db.cursor()
    cur.execute("DELETE FROM links WHERE id=?", (id,))
    db.commit()
    return {"ok": 1}


@app.route("/link/<id>", methods=["PUT"])
def edit(id):
    data = request.get_json()
    title = data.get("title", "")
    url = data.get("url", "")
    db = get_db()
    cur = db.cursor()
    cur.execute(
        "UPDATE links SET title=?, url=? WHERE id=?", (title, url, id))
    db.commit()
    return {"ok": 1}


@app.route("/link/<id>/click", methods=["PUT"])
def click(id):
    db = get_db()
    cur = db.cursor()
    cur.execute(
        "UPDATE links SET clicks=clicks+1 WHERE id=?", (id,))
    db.commit()
    return {"ok": 1}


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
