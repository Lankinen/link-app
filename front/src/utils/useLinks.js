import { useCallback, useState, createContext, useContext, useEffect } from 'react'
import { getLinksApi, addLinkApi, deleteLinkApi, editLinkApi } from './api'

const LinksContext = createContext({})

export const useLinks = () => useContext(LinksContext)

export const LinksContextProvider = ({ children }) => {
  const [links, setLinks] = useState([])

  useEffect(() => {
    const fetchLinks = async () => {
      const initialLinks = await getLinksApi()
      setLinks(initialLinks)
    }

    fetchLinks()
  }, [])

  const addLink = useCallback(async () => {
    const newLink = await addLinkApi()
    setLinks(_links => [..._links, newLink])
  }, [])

  const updateLink = useCallback((id, title, url) => {
    editLinkApi({id, title, url})
    setLinks(_links => {
      return _links.map((link) => {
        return link.id !== id ? link : { ...link, title, url }
      })
    })
  }, [])

  const deleteLink = useCallback((id) => {
    deleteLinkApi(id)
    setLinks(_links => {
      return _links.filter(link => link.id !== id)
    })
  }, [])

  const value = { links, addLink, updateLink, deleteLink }
  return <LinksContext.Provider value={value}>{children}</LinksContext.Provider>
}
