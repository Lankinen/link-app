const root = "http://localhost:5000"

export const getLinksApi = async () => {
  const res = await fetch(`${root}/links/`, {
    method: 'GET'
  })
  const json = await res.json()
  return json["result"].map((item) => ({id: item[0], title: item[1], url: item[2], clicks: item[3]}))
}

export const addLinkApi = async (link) => {
  const res = await fetch(`${root}/link/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(link ||  {})
  })
  const json = await res.json()
  return { id: json["result"], title: "", url: "", clicks: 0 }
}

export const deleteLinkApi = async (id) => {
  await fetch(`${root}/link/${id}`, {
    method: 'DELETE'
  })
}

export const editLinkApi = async (link) => {
  await fetch(`${root}/link/${link.id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(link)
  })
}

export const clickLinkApi = async (id) => {
  await fetch(`${root}/link/${id}/click`, {
    method: 'PUT'
  })
}