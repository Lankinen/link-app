import './App.css';
import { LinksContextProvider } from './utils/useLinks'
import EditorView from './editorView/index';
import DisplayView from './displayView/index';
import { useState } from 'react';

function App() {
  return (
    <LinksContextProvider>
      <div className="App">
        {window.screen.width > 450 ? <Desktop /> : <Mobile />}
      </div>
    </LinksContextProvider>
  );
}

function Desktop() {
  return (
    <>
      <EditorView />
      <DisplayView />
    </>
  )
}

function Mobile() {
  const [toggle, setToggle] = useState(false)
  return (
    <div>
      <button onClick={() => setToggle(!toggle)} style={{marginBottom: 20}}>Toggle</button>
      {toggle ? <EditorView /> : <DisplayView />}
    </div>
  )
}

export default App;
