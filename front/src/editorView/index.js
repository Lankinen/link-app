import './index.css'
import LinkForm from './LinkForm'
import { useLinks } from '../utils/useLinks'

export default function EditorView() {
  const { links, addLink } = useLinks()

  return (
    <div className="EditorView">
      {links.map(link => (
        <LinkForm key={link.id} link={link} />
      ))}
      <button onClick={addLink}>Add new link</button>
    </div>
  )
}