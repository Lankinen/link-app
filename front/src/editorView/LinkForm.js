import { useEffect, useState } from 'react'
import { useLinks } from '../utils/useLinks'
import './LinkForm.css'

export default function LinkForm({ link }) {
  const { updateLink, deleteLink } = useLinks()
  const [title, setTitle] = useState(link.title || "")
  const [url, setUrl] = useState(link.url || "")

  useEffect(() => {
    updateLink(link.id, title, url)
  }, [updateLink, link.id, title, url])

  return (
    <div className="LinkForm">
      <TextField
        onChange={e => setTitle(e.target.value)}
        value={title}
        placeholder="title"
      />
      <TextField
        onChange={e => setUrl(e.target.value)}
        value={url}
        placeholder="url"
      />
      <p>Clicks: {link.clicks}</p>
      <button onClick={() => deleteLink(link.id)}>
        Delete
      </button>
    </div>
  )
}

function TextField({ onChange, value, placeholder }) {
  return (
    <div className="TextField">
      <input onChange={onChange} value={value} placeholder={placeholder}/>
    </div>
  )
}