import { useMemo } from 'react'
import './index.css'
import LinkItem from './LinkItem'
import { useLinks } from '../utils/useLinks'

export default function DisplayView() {
  const { links } = useLinks()
  const filteredLinks = useMemo(() => links.filter(({ title, url }) => title && url), [links])

  return (
    <div className="DisplayView">
      {filteredLinks.map((link) => (
        <LinkItem key={link.id} link={link}/>
      ))}
    </div>
  )
}