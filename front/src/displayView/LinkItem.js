import { useMemo } from "react"
import { clickLinkApi } from "../utils/api"
import './LinkItem.css'

export default function LinkItem({ link }) {
  const { id, title, url } = useMemo(() => link, [link])

  return (
    <div className="LinkItem">
      <a href={url} onClick={() => clickLinkApi(id)}>{title}</a>
    </div>
  )
}