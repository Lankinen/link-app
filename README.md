# Link App

The idea was to create a simple website that allows users to create, read, update, and delete (CRUD) link objects which contain a title and url.

The frontend was developed using React, JavaScript, and CSS. The backend uses Flask, Python, and SQLite.

The time limit on this made me take some shortcuts and there might be bugs I didn't notice. A few big things I would like to improve (I don't list all small things):

- When user edits links, it automatically updates database after every new character or deletion. This causes a lot of unnecessary traffic to the server and it wouldn't most likely scale to even small amount of users.
- I didn't focus seriously to the UI part but tried to implement it as quickly as possible because of the time limit. I did some ugly solutions and with some extra time, it could be possible to improve the UI a lot.
